import org.junit.jupiter.api.Test;
import java.util.List;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ChallengeTest {
    @Test
    public void getStringShouldOutputEvenUnevenString() {
        List<Integer> integerValues = asList(2,3,4,5,6,7);
        assertEquals("e2,o3,e4,o5,e6,o7",Challenge.getString(integerValues));
    }
}