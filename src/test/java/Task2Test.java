import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Task2Test {
    private Task2 testInstance;
    @BeforeEach
    void setUp(){
        testInstance = new Task2();
    }

    @Test
     void shouldReturnNumericLenghtValuesAsList(){
        List<Integer> list = Arrays.asList(0,1,2,3);
        assertEquals(list,testInstance.returnCount("","a","ab","abc"));
    }

//    @ParameterizedTest
//    @MethodSource
//    @ValueSource(ints = [3,4,5,6])
//     void parameterizedTestForTask2(){
//        assertTrue(testInstance.returnCount("abc","abcd","abcd","abcde"));
//    }
}

