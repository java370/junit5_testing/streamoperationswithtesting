import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumAndReduceTest {

    @Test
    public void calculateShouldSumAllNumbers() {
        List<Integer> numbers = asList(1, 2, 3, 4, 5);
        assertEquals((1 + 2 + 3 + 4 + 5),SumAndReduce.calculate(numbers));
    }
}