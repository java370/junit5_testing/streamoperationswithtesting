import java.util.List;
import java.util.stream.*;

public class Task2 {
    public static List<Integer> returnCount(String... names) {
        return Stream.of(names)
                .filter(f -> f.length() > 5)
                .map(m -> m.length())
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(Task2.returnCount("a", "ab", "abc", "abcd", "abcde", "abcdef", "abcdefg", "abcdefgh"));
    }
}