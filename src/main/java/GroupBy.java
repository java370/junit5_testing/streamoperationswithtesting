/*
Group these people by nationality, same kind as the previous exercise
*/

import java.util.*;

import static java.util.stream.Collectors.groupingBy;

public class GroupBy {
    public static Map<String, List<Person>> groupByNationality(List<Person> people) {
        return people.stream()
                .collect(groupingBy(Person::getNationality));
    }
}


//// {
//package streams;
//
//        import java.util.*;
//
//public class GroupBy {
//// }
//
//    public static Map<String, List<Person>> groupByNationality(List<Person> people) {
//        Map<String, List<Person>> map = new HashMap<>();
//        for (Person person : people) {
//            if (!map.containsKey(person.getNationality())) {
//                map.put(person.getNationality(), new ArrayList<>());
//            }
//            map.get(person.getNationality()).add(person);
//        }
//        return map;
//    }
//
//// {
//}
//// }
