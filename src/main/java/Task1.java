import java.util.Collection;
import java.util.stream.*;
public class Task1 {
    public static Collection<String> mapToUppercase(String... names) {
        return Stream.of(names)
                .map(m -> m.toUpperCase())
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Task1.mapToUppercase("hi", "HeLLo", "KILL").forEach(System.out::println);
    }
}
