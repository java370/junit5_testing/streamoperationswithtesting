/*
Write a method that returns a comma separated string based on a given list of integers. Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is odd. For example, if the input list is (3,44), the output should be 'o3,e44'.
*/

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Challenge {
    public static String getString(List<Integer> list) {
        return list.stream()
                .map(m -> m % 2 == 1 ? "o" + m : "e" + m)
                .collect(Collectors.joining(","));
    }
}