/*
Sum all elements of a collection, try to use the reduce operator with identity parameter instead of an IntStream
*/

import java.util.*;

public class SumAndReduce {
    public static int calculate(List<Integer> numbers) {
        return numbers.stream()
                .reduce(0, Integer::sum);
    }
}


//// {
//package streams;
//
//        import java.util.*;
//
//public class SumAndReduce {
//// }
//
//    public static int calculate(List<Integer> numbers) {
//        int total = 0;
//        for (int number : numbers) {
//            total += number;
//        }
//        return total;
//    }
//
//// {
//}
//// }