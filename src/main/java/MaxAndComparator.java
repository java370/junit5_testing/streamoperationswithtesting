/*
Get the oldest person from the collection
*/

import java.util.*;
import java.util.stream.Collectors;

public class MaxAndComparator {
    public static Person getOldestPerson(List<Person> people) {
        return people.stream()
                .max(Comparator.comparing(Person::getAge))
                .get();
    }
}


//// {
//package streams;
//
//        import java.util.*;
//
//public class MaxAndComparator {
//// }
//
//    public static Person getOldestPerson(List<Person> people) {
//        Person oldestPerson = new Person("", 0);
//        for (Person person : people) {
//            if (person.getAge() > oldestPerson.getAge()) {
//                oldestPerson = person;
//            }
//        }
//        return oldestPerson;
//    }
//
//// {
//}
//// }
